var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var UsuarioSchema = new Schema({
  id: {type: Number },
  nombre:  { type: String},
  apellidoPaterno:  { type: String},
  ApellidoMaterno: { type: String },
  email: { type: String},
  user_name: { type: String },
  password: { type: String },
  tipo_usuario: { type: String},
  formato_id: {type: Number},
  mercado_id: {type: Number },
  perfil_id: {type: Number },
  tienda_id: {type: Number},
  flag_externo: {type: Boolean},
  centro_distribucion_id: {type: Number },
  activo:{type: Number},
  token: {type: Number },
  oAuth_token_expired:  {type: Boolean},
  created_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Usuario', UsuarioSchema);