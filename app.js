const sql = require('mssql')
const Usuario = require('./models/usuario.js');
const Utils = require('./helpers/utils.js');
const mongoose = require('mongoose');

const config = {
    user: 'bildadmin',
    password: 'bild$123',
    server: 'bilddev.database.windows.net', // You can use 'localhost\\instance' to connect to named instance
    database: 'walmart_sigo_dev',
 
    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
}

var uri = 'mongodb://localhost:27018/walmart';
//var uri = `mongodb://${IP}:${PORT}/${MONGODB_DATABASE}`;
//var uri = `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_REP}/${MONGODB_DATABASE}?replicaSet=${REPLICA_SET}`


//var uri = 'mongodb://mongodb:27018/movistar';
//var uri = 'mongodb://localhost:27018/movistar';

console.log('uri',uri)
//mongoose.connect(uri, opt).then(
mongoose.connect(uri).then(
  () => { console.log('Connected to Database');},
  err => { 	console.log('ERROR: connecting to Database. ' + err);}
);
 
let conn = async () => {
    console.log('inicio de conexion')
    try {
        await sql.connect(config)
        const result = await sql.query`select * from Usuario`
        
        if(result){
  
           await Utils.asyncForEach(result.recordset, async(usuario) => {

            let ususrioTmp = await saveUserAsync(usuario);
               console.log('respuesta:', ususrioTmp)
   
            });
    }
        
    } catch (err) {
        console.log('error: ', err)
    }
}


conn()
async function saveUserAsync(usuario) {
    User = new Usuario({
        id: usuario.Id,
        nombre: usuario.Nombre,
        apellidoPaterno: usuario.ApellidoPaterno,
        ApellidoMaterno: usuario.ApellidoMaterno,
        email: usuario.Email,
        user_name: usuario.UserName,
        password: usuario.Password,
        tipo_usuario: usuario.TipoUsuario,
        formato_id: usuario.Formato_Id,
        mercado_id: usuario.Mercado_Id,
        perfil_id: usuario.Perfil_Id,
        tienda_id: usuario.Tienda_Id,
        flag_externo: usuario.FlagExterno,
        centro_distribucion_id: usuario.CentroDistribucion_Id,
        activo: usuario.Activo,
        token: usuario.Token,
        oAuth_token_expired: usuario.OAuthTokenExpired
    });

    console.log('usuario a grabar',User)
    User.save(async function (err) {
        if (!err) {
            return 'OK'
        }
        else {
            return 'error'
        }
    });
}

